#!/usr/bin/env bash

apt update
apt install -y python3-pip
cd /vagrant/codingchallenge
pip3 install -r ../requirements.txt
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py loaddata ../user_data.json
python3 manage.py loaddata ../app_data.json
python3 manage.py runserver 0.0.0.0:8000 &
