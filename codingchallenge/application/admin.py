from django.contrib import admin
from .models import Address, UserProfile, Task, Message


admin.site.register([UserProfile, Address, Task, Message])
