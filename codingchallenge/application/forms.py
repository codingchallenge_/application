from django import forms
from django.contrib.auth.models import User


class AuthenticateForm(forms.Form):
    username = forms.CharField(label="username", max_length=30)
    password = forms.CharField(label="password", max_length=30, widget=forms.PasswordInput)

