from django.urls import path
from django.contrib.auth.decorators import login_required

from . import views

urlpatterns = [
    path('', login_required(views.TaskForAddressListView.as_view()), name="wall"),
    path('task/<int:pk>', login_required(views.TaskDetailView.as_view()), name="task_detail"),
    path('profile/', login_required(views.ProfileView.as_view()), name="profile"),
    path('task-create/', login_required(views.TaskCreateView.as_view()), name="task_create"),
    path('assign-view/<int:pk>', views.assign_task_view, name="task_assign"),
    path('assign-auth-view/<int:pk>', views.AuthenticateForChangeTask.as_view(), name="task_authenticate_assign"),
    path('mark-done-view/<int:pk>',views.task_mark_done, name="task_mark_done"),
    path('accept-done-view/<int:pk>',views.task_accept_done, name="task_accept_done"),
    path('history/', login_required(views.ProfileHistoryView.as_view()), name="history"),
    path('messages/', login_required(views.MessagesView.as_view()), name="messages")
]
