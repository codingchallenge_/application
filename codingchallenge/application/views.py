from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.db.models import Q, Sum
from django.views.generic.edit import CreateView, FormView
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required, permission_required
from django import http
from django.urls import reverse_lazy
from django import forms

from .forms import AuthenticateForm
from .models import Task, Address, Message

@login_required
def index(request):
    return render(request, "application/index.html")

@login_required
def assign_task_view(request,pk):
    task=Task.objects.get(pk=pk)
    task.receiver=request.user
    task.status = Task.ASSIGNED
    task.save()
    return redirect("profile")

def task_mark_done(request,pk):
    task=Task.objects.get(pk=pk)
    task.status = Task.DONE
    task.save()
    return redirect("profile")

def task_accept_done(request,pk):
    task=Task.objects.get(pk=pk)
    task.status = Task.APPROVED
    task.save()
    return redirect("profile")

class TaskForAddressListView(ListView):

    def get_queryset(self):
        return Task.objects.filter(
            client__userprofile__address=self.request.user.userprofile.address
        ).filter(receiver__isnull=True)

    context_object_name = "all_tasks"

    template_name = "application/index.html"

class TaskDetailView(DetailView):

    model = Task


class ProfileView(ListView):

    model = Task

    def get_context_data(self, **kwargs):
        u = self.request.user
        context = super().get_context_data(**kwargs)

        context['tasks_client'] = context['tasks'].filter(client=u).exclude(status=Task.APPROVED).order_by('-status')
        context['tasks_receiver'] = context['tasks'].filter(receiver=u).exclude(status=Task.APPROVED).order_by('status')

        plus = Task.objects.filter(receiver=u).filter(status=Task.APPROVED).aggregate(Sum('value'))['value__sum']
        if plus == None:
            plus = 0
        minus = Task.objects.filter(client=u).aggregate(Sum('value'))['value__sum']
        if minus == None:
            minus = 0
        context['user_points'] = plus - minus + 100
        return context

    context_object_name = 'tasks'

    template_name = "application/profile.html"

class ProfileHistoryView(ListView):

    model = Task

    def get_context_data(self, **kwargs):
        u = self.request.user
        context = super().get_context_data(**kwargs)

        context['tasks_client'] = context['tasks'].filter(client=u).filter(status=Task.APPROVED).order_by('-mod_date')
        context['tasks_receiver'] = context['tasks'].filter(receiver=u).filter(status=Task.APPROVED).order_by('-mod_date')
        return context

    context_object_name = 'tasks'

    template_name = "application/history.html"

class TaskCreateView(CreateView):

    model = Task
    fields = ["title", "description", "value"]

    def form_valid(self, form):
        task = form.save(commit=False)
        task.client = self.request.user

        plus = Task.objects.filter(receiver=task.client).filter(status=Task.APPROVED).aggregate(Sum('value'))['value__sum']
        if plus == None:
            plus = 0
        minus = Task.objects.filter(client=task.client).aggregate(Sum('value'))['value__sum']
        if minus == None:
            minus = 0
        points = plus - minus + 100
        if points - task.value >=0:
            task.save()
            return redirect("wall")
        else:
            form.add_error('value', 'Masz za mało punktów, aby dodać to zadanie.')
            return super().form_invalid(form)

    def get_form(self):
        form = super().get_form()
        form.fields['description'].widget = forms.Textarea(attrs={"class": "materialize-textarea"})
        form.fields['value'].widget = forms.NumberInput(attrs={"type": "range"})
        return form

class MessagesView(ListView):

    def get_queryset(self):
        u = self.request.user
        return Message.objects.filter(
            address=u.userprofile.address).order_by('-pub_date')

    context_object_name = "messages"

    template_name = "application/messages.html"


class AuthenticateForChangeTask(FormView):

    template_name = "application/login.html"
    form_class = AuthenticateForm

    def form_valid(self, form):
        cd = form.cleaned_data
        user = authenticate(username=cd["username"], password=cd["password"])
        if user is not None:
            task=Task.objects.get(pk=self.kwargs['pk'])
            task.receiver=user
            task.status = Task.ASSIGNED
            task.save()
            return redirect("wall")
        else:
            return redirect("")
