from django.db import models
from django.contrib.auth.models import User

class Address(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class Task(models.Model):
    title = models.CharField(max_length=50)
    description = models.TextField()
    client = models.ForeignKey(User, on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE, related_name="tasks_assigned")
    value = models.PositiveIntegerField()
    pub_date = models.DateTimeField(auto_now_add=True)
    mod_date = models.DateTimeField(auto_now=True)

    ADDED = 'DODANE'
    ASSIGNED = 'PRZYPISANE'
    DONE = 'WYKONANE'
    APPROVED = 'POTWIERDZONE'

    STATUSES = (
        (ADDED, "Dodane"),
        (ASSIGNED, "Przypisane"),
        (DONE, "Wykonane"),
        (APPROVED, "Potwierdzone"),
    )

    status = models.CharField(
        max_length=12,
        choices = STATUSES,
        default = ADDED
    )

    def __str__(self):
        return self.title

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    flat_number = models.PositiveIntegerField()

    def __str__(self):
        return self.user.username + "'s profile"

class Message(models.Model):
    title = models.CharField(max_length=50)
    content = models.TextField()
    address = models.ForeignKey(Address, on_delete=models.CASCADE, null=True)
    pub_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

